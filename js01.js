var Alumnos = ( function(){
    var _alumnos = [];
  
    function _lista_de(){
       return _alumnos;
    }
  
    function _agregar(obj){
       _alumnos.push(obj);
    }
  
  
    return{
      "lista_de": _lista_de,
      
      "agregar": _agregar
    };
  
  
  });


var Grupos = ( function(){
    var _grupos = [];
  
    function _mostrar(){
       return _grupos;
    }
  
    function _agregar(obj){
       _grupos.push(obj);
    }
  
  
    return{
      "mostrar": _mostrar,
      
      "agregar": _agregar
    };
  
  
  });

var Docentes  = ( function(){
    var _docentes = [];
  
    function _mostrar(){
       return _docentes;
    }
  
    function _agregar(obj){
       _docentes.push(obj);
    }
  
  
    return{
      "mostrar": _mostrar,
      
      "agregar": _agregar
    };
  
  
  });


var Materias = ( function(){
    var _materias = [];
  
    function _mostrar(){
       return _materias;
    }
  
    function _agregar(obj){
       _materias.push(obj);
    }
  
  
    return{
      "mostrar": _mostrar,
      
      "agregar": _agregar
    };
  
  
  });


var Escuela = (function() {
  
  var as = Alumnos ();
  var gr = Grupos();
  var doc = Docentes();
  var ma = Materias();
  
  function _guardar_nuevo_alumno(nc,n,ap,am,g,c=[]){
    as.agregar (
      {
        'numero_de_control':nc, 
          
        'nombre' : n,
        
        'apellido_p' : ap,
        
        'apellido_m' : am,
          
        'genero' : g,
        
        'calificaciones' : c
      } 
      
    );
  }
  
  function _imprimir_lista_de_alumnos() {
    
    console.log('Lista de alumnos');
    var alumnos = as.lista_de();
    for (var i = 0; i < alumnos.length; i++){
         
         console.log(i + '|' + alumnos[i].numero_de_control + '|' + 
                     alumnos[i].nombre + '|' + 
                     alumnos[i].apellido_p + '|'  + 
                     alumnos[i].apellido_m + '|' + alumnos[i].genero + '|'
                    );
         
         }
  }  
  
  function _crea_grupo_nuevo(c,n,a=[]){
    gr.agregar (
      {
        'clave':c, 
          
        'nombre' : n,
        
        'alumnos' : a
               
      } 
      
    );
  }
  
  function _muestra_grupos(){
    
    console.log('Grupos');
    var grupos = gr.mostrar();
    for (var i = 0; i < grupos.length; i++){
         
         console.log(i + '|' + grupos[i].clave + '|' + 
                     grupos[i].nombre + '|' 
                    );
         
         }
  }
  
    function _agrega_docente(r,n,ap,am,g){
    doc.agregar (
      {
        'rfc':r, 
          
        'nombre' : n,
        
        'apellido_p' : ap,
        
        'apellido_m' : am,
          
        'genero' : g,
               
      } 
      
    );
  }
  
    function _muestra_docente(){
    
    console.log('Docentes');
    var docentes = doc.mostrar();
    for (var i = 0; i < docentes.length; i++){
         
         console.log(i + '|' + docentes[i].rfc + '|' + 
                     docentes[i].nombre + '|' + 
                     docentes[i].apellido_p + '|'  + 
                     docentes[i].apellido_m + '|' + docentes[i].genero + '|'
                    );
         
         }
  }
  
   function _crea_materia(c,n){
    ma.agregar (
      {
        'clave':c, 
          
        'nombre' : n
               
      } 
      
    );
  }
  
    
  function _muestra_grupos(){
    
    console.log('Materias');
    var materias = ma.mostrar();
    for (var i = 0; i < materias.length; i++){
         
         console.log(i + '|' + materias[i].clave + '|' + 
                     materias[i].nombre + '|' 
                    );
         
         }
  }
  
  
  
  return{
    "imprimir_lista_de_alumnos": _imprimir_lista_de_alumnos,
    "guardar_nuevo_alumno": _guardar_nuevo_alumno,
    "crea_grupo_nuevo": _crea_grupo_nuevo,
    "muestra_grupos": _muestra_grupos,
    "agrega_docente": _agrega_docente,
    "muestra_docente": _muestra_docente,
    "crea_materia" : _crea_materia,
    "muestra_grupos" : _muestra_grupos
  }
                                                    
})();



